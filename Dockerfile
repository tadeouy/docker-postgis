FROM alpine

ENV LANG en_US.utf8

ENV VERSION "9.6.1"
ENV POSTGIS "2.3.0"

ENV PGDATA /data
ENV PGPASS "postgres"

RUN mkdir ${PGDATA} && chown -R postgres:postgres ${PGDATA}

WORKDIR /tmp
COPY install .
RUN sh install


EXPOSE 5432

CMD ["su-exec", "postgres", "postgres"]
